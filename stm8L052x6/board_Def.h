/*
*
*	MM Board File
*
* (C)2021 Micro Matic 
*			Lars Bo Wassini
*
*
*/


/** DEFINES **/
#define GPIO_HIGH(a,b) 	 a->ODR|=(b)
#define GPIO_LOW(a,b)		 a->ODR&=~(b)
#define GPIO_TOGGLE(a,b) a->ODR^=(b)


#ifdef STM8L05X_MD_VL
	// Micro Matic board

  #define LED_RED_PORT		GPIOE
	#define LED_RED_PIN			GPIO_Pin_1

  #define LED_GREEN_PORT	GPIOE
	#define LED_GREEN_PIN		GPIO_Pin_2
	
	#define BUT_USER_PORT		GPIOC
	#define BUT_USER_PIN		GPIO_Pin_2


	#define RED_ON 		GPIO_LOW(LED_RED_PORT,LED_RED_PIN)
	#define RED_OFF 	GPIO_HIGH(LED_RED_PORT,LED_RED_PIN)
	#define GREEN_ON 	GPIO_LOW(LED_GREEN_PORT,LED_GREEN_PIN)
	#define GREEN_OFF GPIO_HIGH(LED_GREEN_PORT,LED_GREEN_PIN)

#elif STM8L15X_MD

	#define BUT_USER_PORT		GPIOC
	#define BUT_USER_PIN	  GPIO_Pin_1
	
	#define LED_GREEN_PORT	GPIOE
	#define LED_GREEN_PIN		GPIO_Pin_7
		
	// This is actually thr BLUE LED:
	#define LED_RED_PORT		GPIOC
	#define LED_RED_PIN     GPIO_Pin_7

	#define RED_ON 		GPIO_HIGH(LED_RED_PORT,LED_RED_PIN)
	#define RED_OFF 	GPIO_LOW(LED_RED_PORT,LED_RED_PIN)
	#define GREEN_ON 	GPIO_HIGH(LED_GREEN_PORT,LED_GREEN_PIN)
	#define GREEN_OFF GPIO_LOW(LED_GREEN_PORT,LED_GREEN_PIN)


	#define CTN_GPIO_PORT       GPIOC
	#define CTN_CNTEN_GPIO_PIN  GPIO_Pin_4
	#define WAKEUP_GPIO_PORT    GPIOE
	#define ICC_WAKEUP_GPIO_PIN GPIO_Pin_6
	#define ICC_WAKEUP_EXTI_PIN EXTI_Pin_3
//#else
//	#error "Please set a Preprocessor Definition: STM8L05X_MD_VL or STM8L15X_MD"
#endif