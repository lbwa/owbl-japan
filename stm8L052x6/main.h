/*
*
* INFO: Please define STM8L05X_MD_VL as Preprocessor Definitions in settings for MM Production
*
*
*/
#ifndef __MAIN_H
 #define __MAIN_H


// Micro Matic Board: STM8L05X_MD_VL
// ST STM8L Discovery kit: STM8L15X_MD
//#if defined (STM8L15X_MD)
//	#warning "Please define STM8L05X_MD_VL as Preprocessor Definitions in settings for MM Production"
//#endif

#define USE_LSE	(1)


typedef enum {Clean = 0, Dirty = 1} owblstatus;



typedef struct nvrStruct
{
	u8 id;			// must be 42
	u8 days;
	u8 hours;
	u8 minutes;
} nvrDef;









// PROTOTPES

void initGPIO(void);
void initClock(void);
void initEEPROM(void);


void sleep(void);
void blink(void);
void blinkError(u8);
void saveTime(void);
void updateMem(void);
void resetClock(void);

void LSE_StabTime(void);

#ifdef STM8L15X_MD
	void displayTime(void);
#endif

#endif __MAIN_H
