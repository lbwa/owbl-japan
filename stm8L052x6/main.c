/* MAIN.C file
 * 
 * Copyright (c) 2002-2005 STMicroelectronics
 */
#include "stm8l15x.h"
#include "board_def.h"
#include "main.h"
#include "delay.h"
#include <stdio.h>

#ifdef STM8L15X_MD
	#include "stm8l_discovery_lcd.h"
#endif

// --- LOCAL VARIABLES ----------------------------------------------
// Set pointer to EEPROM memory area
nvrDef *pnvr = (nvrDef *)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS;

bool KeyPressed = FALSE;
owblstatus OWBLStatus = Clean;

RTC_DateTypeDef rtc_date;
RTC_TimeTypeDef rtc_time;

__IO uint8_t lastMin;

/***************************************************************
*
* MAIN
*
*/
main()
{
	disableInterrupts();

	initGPIO();
	RED_ON;			// Turn on RED LED until all initialization is done
	GREEN_OFF;

	// Init EEPROM
	if (pnvr->id != 42) // Check for unique ID
	{
		RTC_DateStructInit(&rtc_date); // clear
		RTC_TimeStructInit(&rtc_time);
		saveTime(); // Save empty time/date
	}	

	initClock();

	RED_OFF;	

	#ifdef STM8L15X_MD // Use LCD if using the STM8L Discovery Kit
		LCD_GLASS_Init();
		LCD_GLASS_DisplayString("ALL OK");
		//LCD_bar();
	#endif

	enableInterrupts();


	while (1)
	{
		
		blink(); 
	
		// Reset interrupt flags
		sleep();

		if (KeyPressed)
		{
			disableInterrupts();
			#ifdef STM8L05X_MD_VL
				EXTI_ClearITPendingBit(EXTI_IT_Pin2);
			#else
				EXTI_ClearITPendingBit(EXTI_IT_Pin1);
			#endif
			resetClock();
			KeyPressed = FALSE;
			enableInterrupts();
		}

		//disableInterrupts();
		while (RTC_WaitForSynchro() != SUCCESS);
		RTC_GetTime(RTC_Format_BIN, &rtc_time);

		#ifdef STM8L15X_MD
//		disableInterrupts();
		displayTime();
//		enableInterrupts();
		#endif
		//enableInterrupts();

		// We only want to read the date and update the EEPROM every minute
		if (lastMin != rtc_time.RTC_Minutes)
		{
			//disableInterrupts();
			while (RTC_WaitForSynchro() != SUCCESS);
			RTC_GetDate(RTC_Format_BIN, &rtc_date);
			
			saveTime();
			lastMin = rtc_time.RTC_Minutes;

			if (rtc_date.RTC_Date >= 8)	// Dirty on day 7 (First day is 1 => date==8)
				OWBLStatus = Dirty;
			//enableInterrupts();
		} 

	}
}

/*******************************************************************************************
*	INIT METHODES
********************************************************************************************/

/************************************
*	init GPIO
*/
void initGPIO()
{
	// Init LEDs
	GPIO_Init(LED_RED_PORT, LED_RED_PIN, GPIO_Mode_Out_PP_High_Fast);	
	GPIO_Init(LED_GREEN_PORT, LED_GREEN_PIN, GPIO_Mode_Out_PP_High_Fast);	
	GPIO_Init(BUT_USER_PORT, BUT_USER_PIN, GPIO_Mode_In_FL_IT);	// Init Button
}


/************************************
*	init clock
*/
void initClock()
{
	RTC_InitTypeDef rtc_init;
	RTC_AlarmTypeDef rtc_alarm;
	ErrorStatus errStatus;
	u8 cnt=0;


	// *** INIT CLOCK ***
	CLK_DeInit();							// reset CLK registers

  CLK_LSEConfig(CLK_LSE_ON); /* Enable LSE */

  while (CLK_GetFlagStatus(CLK_FLAG_LSERDY) == RESET); /* Wait for LSE clock to be ready */
  
  LSE_StabTime(); /* wait for 1 second for the LSE Stabilisation */

	CLK_RTCClockConfig(CLK_RTCCLKSource_LSE, CLK_RTCCLKDiv_1);	// Low Speed External (crystal) // Default system clock source is HSI/8

	CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);			// Enable RTC


	RTC_DeInit();

	// *** INIT RTC ***
	RTC_StructInit(&rtc_init);
	errStatus = RTC_Init(&rtc_init);	
	if (errStatus == ERROR) blinkError(2);
	
	RTC_DateStructInit(&rtc_date);
	rtc_date.RTC_Date  = pnvr->days;
	errStatus = RTC_SetDate(RTC_Format_BIN, &rtc_date);
	if (errStatus == ERROR) blinkError(3);

	RTC_TimeStructInit(&rtc_time);
	rtc_time.RTC_Hours = pnvr->hours;
	rtc_time.RTC_Minutes = pnvr->minutes;
	errStatus = RTC_SetTime(RTC_Format_BIN, &rtc_time);
	if (errStatus == ERROR) blinkError(4);

	//while (RTC_WaitForSynchro() != SUCCESS);
}






void sleep()
{

	delay_ms(1000);	// just for debug

	// clear interrupts flags

	//halt();
}



void blink()
{
	// Check state
	if (OWBLStatus == Clean)
	{
		// blink GREEN
		GREEN_ON;
		delay_ms(10);
		GREEN_OFF;
		
	} 
	else
	{
		// blink RED
		RED_ON;
		delay_ms(10);
		RED_OFF;
		delay_ms(300);
		RED_ON;
		delay_ms(10);
		RED_OFF;
	}	
}


/**
  * @brief  Error blink
  * @param  None.
  * @retval None.
  * Note : 
  */
void blinkError(u8 code)
{
	u8 i;
	
	while(1)
	{
		for (i=0; i<10; i++)
		{
			RED_ON;
			if (i<code) GREEN_ON;
			delay_ms(20);
			RED_OFF;
			if (i<code) GREEN_OFF;
			delay_ms(120);
		}
	}
}


/**
  * @brief  save data to EEPROM
  * @param  None.
  * @retval None.
  * Note : 
  */
void saveTime()
{
	//RTC_GetTime(RTC_Format_BIN, &rtc_time);
	
	FLASH_Unlock(FLASH_MemType_Data);
	pnvr->id 	= 42;
	pnvr->days  	= (rtc_date.RTC_Month == RTC_Month_January ? rtc_date.RTC_Date : 8);
	pnvr->hours 	= rtc_time.RTC_Hours;
	pnvr->minutes = rtc_time.RTC_Minutes;
	FLASH_Lock(FLASH_MemType_Data);
}

/**
  * @brief  Reset clock by user
  * @param  None.
  * @retval None.
  * Note : blink 3 times for ok
  */
void resetClock()
{
	u8 i;
	
	RED_OFF;
	GREEN_OFF;
	
	RTC_DateStructInit(&rtc_date); // clear
	RTC_TimeStructInit(&rtc_time);
	
	RTC_SetDate(RTC_Format_BCD, &rtc_date);
	RTC_SetTime(RTC_Format_BCD, &rtc_time);
	
	saveTime();
	
	for (i=0; i<3; i++)
	{
		GREEN_ON;
		delay_ms(50);
		GREEN_OFF;
		delay_ms(50);
	}
}



/**
  * @brief  Wait 1 sec for LSE stabilization .
  * @param  None.
  * @retval None.
  * Note : TIM4 is configured for a system clock = 2MHz
  */
void LSE_StabTime(void)
{

  CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);

  /* Configure TIM4 to generate an update event each 1 s */
  TIM4_TimeBaseInit(TIM4_Prescaler_16384, 123);
  /* Clear update flag */
  TIM4_ClearFlag(TIM4_FLAG_Update);

  /* Enable TIM4 */
  TIM4_Cmd(ENABLE);

  /* Wait 1 sec */
  while ( TIM4_GetFlagStatus(TIM4_FLAG_Update) == RESET );

  TIM4_ClearFlag(TIM4_FLAG_Update);

  /* Disable TIM4 */
  TIM4_Cmd(DISABLE);

  CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, DISABLE);
}


#ifdef STM8L15X_MD
void displayTime()
{
	char timestr[]="hhmmss"; // d-hhmm

//	sprintf(timestr, "%1d-%02d%02d", rtc_date.RTC_Date < 8 ? rtc_date.RTC_Date : 8, rtc_time.RTC_Hours  & 0xff, rtc_time.RTC_Minutes  & 0xff);
	sprintf(timestr, "%02d%02d%02d", rtc_time.RTC_Hours  & 0xff, rtc_time.RTC_Minutes  & 0xff, rtc_time.RTC_Seconds  & 0xff);
		
	LCD_GLASS_DisplayString(timestr);

}
#endif













